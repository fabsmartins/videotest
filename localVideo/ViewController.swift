//
//  ViewController.swift
//  localVideo
//
//  Created by Fabio Martins on 10/19/18.
//  Copyright © 2018 Fabio Martins. All rights reserved.
//

import UIKit
import AVKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    @IBAction func pressToWatch(_ sender: UIButton) {
        if let path = Bundle.main.path(forResource: "MOV_5747", ofType: "MOV") {// get URL to the video
            let video = AVPlayer(url: URL(fileURLWithPath: path)) //access video itself
            let videoPlayer = AVPlayerViewController() // create player view controller
            videoPlayer.player = video //select the video to be played
            
            present(videoPlayer, animated: true) { //full screen Apple video Presentation
                
                video.play()//activate player
            }
            
        }
        
    }
    
}


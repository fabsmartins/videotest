//
//  VC2.swift
//  localVideo
//
//  Created by Fabio Martins on 10/19/18.
//  Copyright © 2018 Fabio Martins. All rights reserved.
//

//Video as Layer

import UIKit
import AVKit //needed for playback

class VC2: UIViewController {

    let path = Bundle.main.path(forResource: "MOV_5747", ofType: "MOV") //access video itself
    var player: AVPlayer! = nil
    var playerLayer: AVPlayerLayer! = nil



    
    override func viewDidLoad() {
        super.viewDidLoad()
        player = AVPlayer(url: URL(fileURLWithPath: self.path!)) //setup player for the selected video
        playerLayer = AVPlayerLayer(player: player) // let the layer contain the video player
        playerLayer.videoGravity = .resizeAspect //resizes the layer to execute video playing
        videoViewerLayer.layer.addSublayer(playerLayer)

        // Do any additional setup after loading the view.
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        player.play()//play on appear
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame = videoViewerLayer.bounds//when layout changes, change frame
    }
    
    @IBOutlet weak var videoViewerLayer: UIView!
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

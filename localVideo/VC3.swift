//
//  VC3.swift
//  localVideo
//
//  Created by Fabio Martins on 10/19/18.
//  Copyright © 2018 Fabio Martins. All rights reserved.
//

import UIKit
import AVKit

class VC3: UIViewController {
    
    @IBOutlet weak var videoView: UIView!
    
    let path = Bundle.main.path(forResource: "MOV_5747", ofType: "MOV") //access video itself
    var player: AVPlayer! = nil //setup for video player
    var playerLayer: AVPlayerLayer! = nil //setup for the layer that will play the video
    var videoPlayer = AVPlayerViewController() //ViewController with all of Apple's Video player controls
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        player = AVPlayer(url: URL(fileURLWithPath: self.path!))//setup player to play media
        videoPlayer.player = player //setup the same player for the layer and Apple's VC
        playerLayer = AVPlayerLayer(player: player)//fill the layer withthe media player
        playerLayer.videoGravity = .resizeAspect //resizes the layer to execute video playing
        videoView.layer.insertSublayer(playerLayer, at: 0)//populate videoView with the layer of the video
        
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        playerLayer.frame = videoView.bounds//when layout changes, change frame
    }
    
    @IBAction func playButton(_ sender: UIButton) {
        player.play()
    }
    
    @IBAction func fullScreenButton(_ sender: UIButton) {
        present(videoPlayer, animated: false, completion: nil)
    }
    

}
